﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;

namespace Patient_Self_Register.Models
{
    /// <summary>
    /// Purpose: This class will be responsible for encrypting passwords strings
    ///          to provide additional security incase the database is ever compromised. 
    /// <remarks>
    /// Date: 10/28/2012
    /// Author: Jerad Godreault
    /// </remarks>
    /// </summary>
    public class Cryptography
    {

        /// <summary>
        ///     Purpose: To encode an String into sha256 hash password. 
        /// <remarks>
        ///     Based on the sample Rick van den Bosch provided
        ///     <see cref="http://bloggingabout.net/blogs/rick/archive/2005/05/18/4118.aspx"/>
        /// </remarks>
        /// </summary>
        /// <param name="originalPassword">An String  that needs to be encrypted</param>
        /// <returns></returns>
        public static string EncodePassword(string originalPassword, string salt)
        {
            //Declarations
            Byte[] passwordBytes;
            Byte[] encodedBytes;
            SHA256 sha256 = new SHA256Managed();

            passwordBytes = ASCIIEncoding.Default.GetBytes(originalPassword + salt);

            encodedBytes = sha256.ComputeHash(passwordBytes);

            //Convert encoded bytes back to a 'readable' string
            return BitConverter.ToString(encodedBytes);
        }

        /// <summary>
        /// Purpose: To generate an salt that can be used for authentication 
        ///  <see cref="http://stackoverflow.com/questions/2138429/hash-and-salt-passwords-in-c-sharp"/>
        /// </summary>
        /// <param name="size">the size of the salt</param>
        /// <returns></returns>
        public static string CreateSalt(int size)
        {
            //Generate a cryptographic random number.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);

            // Return a Base64 string representation of the random number.
            return Convert.ToBase64String(buff);
        }


    }
}