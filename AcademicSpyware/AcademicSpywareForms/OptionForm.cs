﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AcademicSpywareForms
{
    /// <summary>
    ///     Purpose: Just an indicator on whos machine this belongs
    /// </summary>
    public partial class OptionForm : Form
    {
        public OptionForm()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Purpose: sets the textbox to the Machine Name of the computer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptionForm_Load(object sender, EventArgs e)
        {
            this.textBoxMachineName.Text = Environment.MachineName;
        }



    }
}
