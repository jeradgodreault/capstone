﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicSpywareWebService.Models
{
    /// <summary>
    ///         Purpose: Matches the key from the database table Status
    /// </summary>
    public enum StatusType
    {
        unavailable = 10,
        idle = 11,
        shutdown = 12,
        available = 16,
        unknown = 17
    }
}
