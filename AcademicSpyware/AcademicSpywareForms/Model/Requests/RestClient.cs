﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AcademicSpywareWebService.Models;

namespace AcademicSpywareForms.RestRequests
{ 

    /// <summary>
    ///     Purpose: This class will make REST calls to a service 
    ///     
    /// </summary>
    public class RestClient
    {
        public String baseUrl { get; set; }
        public HttpClient client;
        public String resourcePath { get; set; }
        public HttpResponseMessage response { get; private set; }
        public ProtocolType httpProtocol { get; set; }

        /// <summary>
        ///     Purpose: Used to send a post payload with a custom object. 
        /// </summary>
        public Object postObject { get; set; }


        /// <summary>
        ///     Purpose: The constructor for RestClient.
        /// <remarks>
        ///     Note: Default is to use application/json.
        ///     
        ///     If ProtocolType not specified, defaults to GET
        /// </remarks>
        /// </summary>
        public RestClient()
        {
            client = new HttpClient();

            // Add an Accept header for JSON format.
            this.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        /// <summary>
        ///     Purpose: To execute an request based on the specified 
        /// <remarks>
        ///    If ProtocolType not specified, defaults to GET
        /// </remarks>
        /// </summary>
        public async void Execute()
        {
            client.BaseAddress = new Uri(this.baseUrl);

            response = await performRequest();
        }

        /// <summary>
        /// Purpose: Construct the message to be send to the webservice
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> ExecuteAsync()
        {
            client.BaseAddress = new Uri(this.baseUrl);

            return await AccessTheWebAsync();
        }

        /// <summary>
        /// TODO: Remove
        /// </summary>
        /// <returns></returns>
        async Task<HttpResponseMessage> AccessTheWebAsync()
        {
            return await performRequest();
        }



        /// <summary>
        /// Purpose: Sends the actual message to the webservice based on the 
        ///          ProtocolType
        /// </summary>
        /// <returns>The message from the webservice</returns>
        private async Task<HttpResponseMessage> performRequest()
        {
            switch (httpProtocol)
            {
                case ProtocolType.POST:
                {
                    return await this.client.PostAsJsonAsync(this.resourcePath, this.postObject).ConfigureAwait(false);
                }
                case ProtocolType.GET:
                default:
                {
                    return await this.client.GetAsync(this.resourcePath);
                }
            }
        }


    }



}
