﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AcademicSpywareForms.Model;
using AcademicSpywareForms.RestRequests;
using AcademicSpywareWebService.Models;
using Microsoft.Win32;

namespace AcademicSpywareForms
{
    /// <summary>
    ///     Purpose: Enable a demonstration of tracking computer states and 
    ///              send of to a web service
    ///     
    /// </summary>
    public partial class MainForm : Form
    {
        private SystemService systemComputer;
        

        /// <summary>
        /// Purpose: initialize component
        /// </summary>
        public  MainForm()
        {
            InitializeComponent();
            setup();
        }

        /// <summary>
        /// Purpose: an asycn method that setups variable that done through a webservice
        /// </summary>
        private async void setup()
        {
            systemComputer = new SystemService(Environment.MachineName);
           await whoami();
        }

        /// <summary>
        ///     Purpose: This will determine where the computer is located in the school and
        ///              try to communicate to the webservice to get an ID.
        /// </summary>
        /// <returns>returns TRUE if the systemComputer has a ID</returns>
        private async Task<Boolean> whoami()
        {
            Boolean hasComputerID = false;

            if (systemComputer.computer.ID != 0)
            {
                hasComputerID = true;
            }
            else
            {
                try
                {
                    Requests request = new Requests();
                    var t = await request.PostComputerAsyncVersionTwo(systemComputer.computer);

                    if (t != null)
                    {
                        systemComputer.computer = t;

                        Debug.WriteLine("whoami={0} | {1}", systemComputer.computer.ID, systemComputer.computer.Name);

                        Debug.WriteLine("");

                        hasComputerID = true;
                    }

                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception was thrown {0}", ex);
                    hasComputerID = false;
                }
            }

            return hasComputerID;
        }


        /// <summary>
        ///     Purpose: Add our self to start listening on session delegates 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            toggleButton();

            SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);
            SystemEvents.SessionSwitch += new SessionSwitchEventHandler(SystemEvents_SessionSwitching);
        }

        /// <summary>
        /// Purpose: remove our self from listening on session delegates 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStop_Click(object sender, EventArgs e)
        {
            toggleButton();

            SystemEvents.SessionEnding -= new SessionEndingEventHandler(SystemEvents_SessionEnding);
            SystemEvents.SessionSwitch -= new SessionSwitchEventHandler(SystemEvents_SessionSwitching);
        }

        private void SystemEvents_SessionSwitching(object sender, SessionSwitchEventArgs e)
        {
            Debug.WriteLine("System SessionSwitching ={0}", e.Reason);

            if (whoami().Result == true)
            {
                switch (e.Reason)
                {
                    case SessionSwitchReason.SessionLock:
                    case SessionSwitchReason.SessionLogoff:
                    case SessionSwitchReason.ConsoleDisconnect:
                    {
                        sendRequest(StatusType.available);
                        break;
                    }
                    case SessionSwitchReason.ConsoleConnect:
                    case SessionSwitchReason.SessionUnlock:
                    case SessionSwitchReason.SessionLogon:
                    {
                        sendRequest(StatusType.unavailable);
                        break;
                    }
                }
            }
            else
            {
                Debug.WriteLine("I don't even know who I am");
            }
        }


        /// <summary>
        ///     Purpose: Sends the request about the history change
        /// </summary>
        /// <param name="statusEvent">The status for the event of history</param>
        private void sendRequest(StatusType statusEvent)
        {
            this.systemComputer.currentStatus = statusEvent;
    
            try
            {
                History historyEvent = new History()
                {
                    LastUpdate = DateTime.Now,
                    StatusID = (int)statusEvent,
                    ComputerID = this.systemComputer.computer.ID
                };

                Requests request = new Requests();
                request.PostHistoryAsync(historyEvent);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Could not post to the server. Internet connection may be down or an internal error might have occurred "+ ex);
            }
        }

        /// <summary>
        ///     Purpose: Detect when the O.S is shutting down or logging off
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {
            Debug.WriteLine("System SessionSwitching ={0}", e.Reason);

            if (whoami().Result == true)
            {
                switch (e.Reason)
                {
                    case SessionEndReasons.SystemShutdown:
                    {
                        sendRequest(StatusType.shutdown);
                        break;
                    }
                    case SessionEndReasons.Logoff:
                    {
                        sendRequest(StatusType.available);
                        break;
                    }
                }
            }

        }




        /// <summary>
        ///     Purpose: Toggles the Start and Stop button 
        /// </summary>
        private void toggleButton()
        {
            buttonStart.Enabled = !buttonStart.Enabled;
            buttonStop.Enabled = !buttonStop.Enabled;
        }

        /// <summary>
        ///     Purpose: Just show an option page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            OptionForm form = new OptionForm();
            form.ShowDialog();
        }

    }
}
