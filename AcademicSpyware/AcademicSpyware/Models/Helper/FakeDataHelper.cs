﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AcademicSpyware.Models;

namespace AcademicSpyware.Models.Helper
{
    /// <summary>
    /// Purpose: To generate fake data to get a better understand how 
    ///          things will look will over 1000 records. 
    /// </summary>
    public class FakeDataHelper
    {
      
        private SpywareDBEntities db = new SpywareDBEntities();
        List<Status> allStatus;


        enum statusType
        {
            available = 0,
            unavailable = 1,
            idle = 2,
            shutdown = 3,
            unknown = 4
        }

       
         public FakeDataHelper()
         {
             allStatus = new List<Status>();
         }




        /// <summary>
        /// Generate computers based on the format of ufe-e202-pc38’ 
        /// </summary>
        public void generateComputers()
        {
            List<Computer> computers = new List<Computer>();
            char[] building = new char[] { 'C', 'F', 'H', 'J', 'I' }; //the common computer in Mohawk College
            Random random = new Random();


            //Labs in each building
            foreach (char b in building)
            {
                Computer newComputer = new Computer();

                int maxRoom = random.Next(3, 8);
                
                //Randomly number of computer labs
                for (int i = 0; i < maxRoom; i++)
                {
                    //every other one will be either ground or top floor
                    int roomNumber = ((i % 2) == 0) ? random.Next(100, 150) : random.Next(200, 250);

                    int computerPerRoom = random.Next(5, 15); //min 10, max 29 computers

                    for (int j = 1; j < computerPerRoom; j++)
                    {
                        newComputer = new Computer()
                        {
                            Building = b.ToString(),
                            Name = j.ToString(),
                            Room = roomNumber.ToString(),
                            isEnabled = true
                        };

                        computers.Add(newComputer); // to be used for history
                        db.Computers.Add(newComputer); //adds to the entity
                    }
                }
            }

            //insert the entity to the database
            db.SaveChanges();


            foreach (Computer c in computers)
            {
                generateHistory(c);
            }

        }


        /// <summary>
        /// Purpose: Create a fake history for a specific a certain day
        /// </summary>
        /// <param name="computer"></param>
        public void generateHistory(Computer computer)
        {
            Random random = new Random();

            //List<History> historyList = new List<History>();
            DateTime dateTime = DateTime.Now;

            for (DateTime dt = DateTime.Now.AddDays(-1); dt.Date < dateTime.Date; dt = dt.AddDays(1))
            {
                TimeSpan ts = new TimeSpan(8, 0, 0);

                for (int hour = ts.Hours; hour < 23; hour++)
                {
                    int index = random.Next(0,allStatus.Count());

                    int statusIndex = probablityByHour(hour);


                    History history = new History()
                    {
                        LastUpdate = dt.AddHours(hour),
                        ComputerID = computer.ID,
                        StatusID = allStatus[0].ID
                    };

                    //historyList.Add(history);

                    db.Histories.Add(history);
                }

            }

            //insert the entity to the database
            db.SaveChanges();

        }


        /// <summary>
        /// Not used
        /// </summary>
        /// <param name="hour"></param>
        /// <returns></returns>
        private int probablityByHour(int hour)
        {
            int statusIndex = (int)statusType.unknown; //initlize as unknown


            if (hour < 10)
            {
                //
            }
            else if (hour > 10 && hour < 1)
            {
                //96 chance unavailable 


            }
            else if (hour > 13 && hour < 15)
            {

            }
            else if (hour < 18)
            {

            }
            else if (hour > 20 && hour < 21)
            {

            }
            else if (hour > 21 && hour < 22)
            {

            }
            else
            {
                //95 % shutdown

            }

            return 34;
        }



        /// <summary>
        /// Purpose Create statuses for the database
        /// </summary>
        public void generateStatus()
        {
            List<Status> status = new List<Status>();

            status.Add(new Status()
            {
                ID = (int)statusType.available,
                Description = statusType.available.ToString()
            });
            status.Add(new Status()
            {
                ID = (int)statusType.unavailable,
                Description = statusType.unavailable.ToString()
            });
            status.Add(new Status()
            {
                ID = (int)statusType.idle,
                Description = statusType.idle.ToString()
            });
            status.Add(new Status()
            {
                ID = (int)statusType.shutdown,
                Description = statusType.shutdown.ToString()
            });
            status.Add(new Status()
            {
                ID = (int)statusType.unknown,
                Description = statusType.unknown.ToString()
            });

            foreach (Status s in status)
            {

                db.Status.Add(s);
                allStatus.Add(s);
            }

            db.SaveChanges();

        }

        

  



    }
}