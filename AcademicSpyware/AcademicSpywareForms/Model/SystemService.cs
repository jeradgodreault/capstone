﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AcademicSpywareWebService.Models;

namespace AcademicSpywareForms.Model
{
    /// <summary>
    ///     Purpose: Keep track of the current system computer and status
    /// </summary>
    public class SystemService
    {
        public Computer computer {get; set;}
        public StatusType currentStatus { get; set; }
       
        /// <summary>
        ///     Purpose: Splits the location from the style of ufe-i103-pc03.
        ///              If its not in this format it will default to Unknown building
        /// </summary>
        /// <param name="location">The location that the computer is located</param>
        public SystemService(String location)
        {
            computer = new Computer();

            var section = location.Split('-');

            if (section.Length > 2)
            {
                computer.Building = section[1][0].ToString();
                computer.Room = section[1].Substring(1);
                computer.Name = section[2];
            }
            else
            {
                computer.Building = "Unknown";
                computer.Room = "N/A";
                computer.Name = location;
            }
        }
    }
}
