﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcademicSpywareWebService.Controllers;
using AcademicSpywareWebService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcademicSpywareWebService.Tests.Controllers
{
    [TestClass]
    public class WebServiceTest
    {

        /// <summary>
        /// Purpose: Test to ensure a database is 
        ///          populated and returns multiple results of IEnumberable of computers.
        /// </summary>
        [TestMethod]
        public void GetAllComputers()
        {
            ComputerController apiController = new ComputerController();

            var result = apiController.GetComputers();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IEnumerable<Computer>));
            Assert.IsTrue(result.Count() > 0);
        }


        /// <summary>
        /// Purpose: Ensure that a new computer (or existing)
        ///          is return with its ID. The ID is used 
        /// </summary>
        [TestMethod]
        public Computer PostNewComputer()
        {
            ComputerController apiController = new ComputerController();

            Computer computer = new Computer()
            { 
                   Building = "A",
                   Room = "104",
                   Name = "29"
            };

            //The Server will return Computer from the database, that
            // should contain a computer ID
            var result = apiController.PostComputerEntity(computer);

            Assert.IsNotNull(result);
            Assert.AreEqual(computer.Room, result.Room);
            Assert.AreEqual(computer.Name, result.Name);
            Assert.AreEqual(computer.Building, result.Building);
            Assert.IsNotNull(result.ID); 
            Assert.IsTrue(result.ID > 0); //We should have ID now


            return result;
        }


        /// <summary>
        /// Purpose: If passed in already existing Computer,
        ///          the service should NOT create a new one
        /// </summary>
        [TestMethod]
        public void PostCheckExistingComputer()
        {
            ComputerController apiController = new ComputerController();

            Computer computer = new Computer()
            {
                Building = "A",
                Room = "104",
                Name = "29"
            };

            var result = apiController.PostComputerEntity(computer);

            Assert.IsNotNull(result.ID); 

            var existingResult = apiController.PostComputerEntity(result);


            Assert.AreEqual(result.ID, existingResult.ID);
            Assert.AreEqual(result.Room, existingResult.Room);
            Assert.AreEqual(result.Name, existingResult.Name);
            Assert.AreEqual(result.Building, existingResult.Building);


        }

        /// <summary>
        ///     Purpose: Checks to see if you can submit empty data, 
        /// </summary>
        [TestMethod]
        public void PostCheckEmptyComputerData()
        {
            ComputerController apiController = new ComputerController();

            Computer computer = new Computer();

            var result = apiController.PostComputerEntity(computer);

            Assert.IsNull(result.Building);
            Assert.IsNull(result.Name);
            Assert.IsNull(result.Room);
            Assert.AreEqual(0, result.ID); // ints are deaults to 0
        }

        /// <summary>
        ///     Purpose: Post a history of available
        /// </summary>
        [TestMethod]
        public void PostHistoryAvailable()
        {
                                //get computer to post history to
            Computer computer = PostNewComputer();
            
            HistoryController apiController = new HistoryController();

            History history = new History()
            {
                ComputerID = computer.ID,
                LastUpdate = DateTime.Now,
                StatusID = (int)StatusType.available
            };

            var result =  apiController.PostHistoryEntity(history);

            Assert.AreEqual(result.ComputerID, history.ComputerID);
            Assert.AreEqual(result.StatusID, (int)StatusType.available);
            Assert.IsTrue(result.ID > 0);
        }


        /// <summary>
        /// Purpose:    Post a history of unavailable
        /// </summary>
        [TestMethod]
        public void PostHistoryUnavailable()
        {
            //get computer to post history to
            Computer computer = PostNewComputer();

            HistoryController apiController = new HistoryController();

            History history = new History()
            {
                ComputerID = computer.ID,
                LastUpdate = DateTime.Now,
                StatusID = (int)StatusType.unavailable
            };

            var result = apiController.PostHistoryEntity(history);

            Assert.AreEqual(result.ComputerID, history.ComputerID);
            Assert.AreEqual(result.StatusID, (int)StatusType.unavailable);
            Assert.IsTrue(result.ID > 0);
        }

        /// <summary>
        ///     Purpose: Post a history of idle
        /// </summary>
        [TestMethod]
        public void PostHistoryIdle()
        {
            //get computer to post history to
            Computer computer = PostNewComputer();

            HistoryController apiController = new HistoryController();

            History history = new History()
            {
                ComputerID = computer.ID,
                LastUpdate = DateTime.Now,
                StatusID = (int)StatusType.idle
            };

            var result = apiController.PostHistoryEntity(history);

            Assert.AreEqual(result.ComputerID, history.ComputerID);
            Assert.AreEqual(result.StatusID, (int)StatusType.idle);
            Assert.IsTrue(result.ID > 0);
        }

        /// <summary>
        ///     
        /// </summary>
        [TestMethod]
        public void PostHistoryCheckCorrect()
        {
            //get computer to post history to
            Computer computer = PostNewComputer();

            HistoryController apiController = new HistoryController();

            History history = new History()
            {
                ComputerID = computer.ID,
                LastUpdate = DateTime.Now,
                StatusID = (int)StatusType.available
            };

            var result = apiController.PostHistoryEntity(history);

            Assert.AreEqual(result.ComputerID, history.ComputerID);
            Assert.AreNotEqual(result.StatusID, (int)StatusType.unavailable);
            Assert.IsTrue(result.ID > 0);
        }


    }
}
