﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcademicSpyware.Models;

namespace AcademicSpyware.Controllers
{
    /// <summary>
    ///     Purpose: The admin controller that will enable user to edit
    /// <remarks>
    ///     Authorize: Will be redirect to login if not login    
    /// </remarks>
    /// </summary>
    [Authorize]
    public class AdminController : Controller
    {
        private SpywareDBEntities db = new SpywareDBEntities();

        
        /// <summary>
        ///     Purpose: Shows a list of computers 
        /// </summary>
        /// <returns> GET: /Admin/</returns>
        public ActionResult Index()
        {
            return View(db.Computers.ToList());
        }


        /// <summary>
        ///     Purpose: Shows a computer model that can be edited
        /// </summary>
        /// <param name="id">an computerID</param>
        /// <returns>GET: /Admin/Details/5</returns>
        public ActionResult Details(int id = 0)
        {
            Computer computer = db.Computers.Find(id);
            if (computer == null)
            {
                return HttpNotFound();
            }
            return View(computer);
        }

        
        /// <summary>
        /// Purpose: To create new computers
        /// </summary>
        /// <returns> GET: /Admin/Create</returns>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Purpose : Submit the form computer to the database
        /// </summary>
        /// <param name="computer"></param>
        /// <returns> /Admin/Create</returns>
        [HttpPost]
        public ActionResult Create(Computer computer)
        {
            if (ModelState.IsValid)
            {
                db.Computers.Add(computer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(computer);
        }


        /// <summary>
        ///     Purpose: allows to update a computer
        /// </summary>
        /// <param name="id"></param>
        /// <returns> GET: /Admin/Edit/5</returns>
        public ActionResult Edit(int id = 0)
        {
            Computer computer = db.Computers.Find(id);
            if (computer == null)
            {
                return HttpNotFound();
            }
            return View(computer);
        }

        

        /// <summary>
        /// Purpose: Submit the updated computer to the database
        /// </summary>
        /// <param name="computer"></param>
        /// <returns>POST: /Admin/Edit/5</returns>
        [HttpPost]
        public ActionResult Edit(Computer computer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(computer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(computer);
        }

       
        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}