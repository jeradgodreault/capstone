﻿using System.Web;
using System.Web.Mvc;

namespace AcademicSpyware
{
    /// <summary>
    /// Purpose: Register the Error handler so the pages will be redirected to a custom error message
    /// </summary>
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}