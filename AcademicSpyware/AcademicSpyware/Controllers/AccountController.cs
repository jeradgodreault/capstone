﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AcademicSpyware.Models;
using Patient_Self_Register.Models;

namespace AcademicSpyware.Controllers
{
    /// <summary>
    ///     Purpose: Allow user to register/login or log off
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        private SpywareDBEntities db = new SpywareDBEntities();
        
        /// <summary>
        ///     Purpose: Show the login page
        /// </summary>
        /// <param name="returnUrl">an return address</param>
        /// <returns>GET: /Account/Login</returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }


        /// <summary>
        /// Purpose: If valid loginModel, submit the user to the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns>POST: /Account/Login</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {

                if (ValidateUser(model.UserName, model.Password) != null)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }


        /// <summary>
        ///     Purpose: Validate the user username and credentials to authorize them.
        /// </summary>
        /// <remarks>
        ///     Date: 11/30/2012
        /// </remarks>
        /// <param name="username">An User username </param>
        /// <param name="inputPassword">an User password. This will be encrypt</param>
        /// <returns>An User from database</returns>
        private User ValidateUser(String username, String inputPassword)
        {
            User user = db.Users.Where(w => w.username == username).FirstOrDefault();


            if (user != null)
            {
                String encryptPassword = Cryptography.EncodePassword(inputPassword, user.salt);
                if (user.password == encryptPassword)
                {
                    return user;
                }
            }

            return null;
        }


        /// <summary>
        ///     Purpose: Checked the database if the username already exists. 
        /// </summary>
        /// <returns>
        ///     Date: 11/30/2012
        ///     True if the username already exists. 
        ///</returns>
        public Boolean IsUserExists(string userName)
        {
            User user = db.Users.Where(m => m.username == userName).FirstOrDefault();

            return (user != null) ? true : false;
        }




        /// <summary>
        ///     Purpose: to submit a new account to the database
        /// </summary>
        /// <param name="model">Registration that will submit to the database</param>
        /// <returns>POST: /Account/Register</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                 User newUser = new User();
                    newUser.username = model.Email;
                    newUser.salt = Cryptography.CreateSalt(50);
                    newUser.password = Cryptography.EncodePassword(model.Password, newUser.salt);

                    if (IsUserExists(newUser.username) == false)
                    {
                        db.Users.Add(newUser);

                        int i = db.SaveChanges();

                        if (i > 0)
                        {
                            FormsAuthentication.SetAuthCookie(model.Email, createPersistentCookie: false);
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Could not insert new user. Try again later");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "User already exists. Please create a different one");
                    }
            }

            

            // If we got this far, something failed, redisplay form
            return View(model);
        }



    }
}
