﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcademicSpywareForms.RestRequests
{


    /// <summary>
    ///     Purpose: Different HTTP protocol you can use for REST.
    /// </summary>
    public enum ProtocolType
    {
        GET,
        POST,
        PUT,
        DELETE,
        UPDATE
    }

}
