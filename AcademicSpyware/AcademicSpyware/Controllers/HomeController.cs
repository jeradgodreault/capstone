﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcademicSpyware.Models.Helper;

namespace AcademicSpyware.Controllers
{
    /// <summary>
    ///     Purpose: Shows generic items
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Purpose Shows the HomePage with the awesome 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Message = "";

            return View();
        }

        /// <summary>
        /// Shows a about page
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        /// <summary>
        /// Shows the contact information of Jerad
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// For testing onlym generate fake data
        /// </summary>
        /// <returns></returns>
        public ActionResult TemporaryData()
        {
            FakeDataHelper fakeData = new FakeDataHelper();
            fakeData.generateStatus();
            fakeData.generateComputers();
          
            return View();
        }

    }
}
