﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AcademicSpywareWebService.Models;
using AcademicSpywareWebService.Models.Database;

namespace AcademicSpywareWebService.Controllers
{
    public class HistoryController : ApiController
    {
        private SpywareEntities db = new SpywareEntities();

        
        /// <summary>
        ///     Purpose: Get all the history from the database
        /// <remarks>
        ///     Limit: Only Take 15 history at at time
        ///     GET api/History
        /// </remarks>
        /// </summary>
        public IEnumerable<History> GetHistoryEntities()
        {
            var historyentities = db.HistoryEntities.Include("Computer").Include("Status");


            var history2 = db.HistoryEntities.Select(entity => new History
            {
                ID = entity.ID,
                ComputerID = entity.ComputerID,
                StatusID = entity.StatusID,
                LastUpdate = entity.LastUpdate
            }).Take(15);

            return history2.AsEnumerable();
        }


        /// <summary>
        /// Purpose: Accepts a json history message and submits into the database
        /// POST api/History
        /// </summary>
        /// <param name="history">An history json message to be submitted</param>
        /// <returns>An history that was just submitted to the database</returns>
        public History PostHistoryEntity(History history)
        {

            if (ModelState.IsValid)
            {
                var entity = new HistoryEntity()
                {
                    ComputerID = history.ComputerID,
                    StatusID = history.StatusID,
                    LastUpdate = DateTime.Now
                };


                db.HistoryEntities.AddObject(entity);
                db.SaveChanges();

                history.ID = entity.ID;
                history.StatusID = entity.StatusID;

                History convertHistory = new History()
                {
                    ID = entity.ID,
                    ComputerID = entity.ComputerID,
                    LastUpdate = entity.LastUpdate,
                    StatusID = entity.StatusID
                };

                return convertHistory;
            }
            else
            {
                return history;
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}