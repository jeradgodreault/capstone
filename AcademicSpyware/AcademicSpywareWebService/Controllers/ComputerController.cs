﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AcademicSpywareWebService.Models;
using AcademicSpywareWebService.Models.Database;


namespace AcademicSpywareWebService.Controllers
{
    public class ComputerController : ApiController
    {
        private SpywareEntities db = new SpywareEntities();

        
        /// <summary>
        /// Purpose: Shows a list of computer in the database into JSON format
        /// 
        /// GET api/Computer
        /// </summary>
        /// <returns>A list of computers in JSON format</returns>
        public IEnumerable<Computer> GetComputers()
        {
            IEnumerable<Computer> list = db.ComputerEntities.
                Select(entity => new Computer
                {
                    ID = entity.ID,
                    Building = entity.Building,
                    isEnabled = entity.isEnabled,
                    Name = entity.Name,
                    Room = entity.Room
                });

            return list;
        }


        /// <summary>
        /// Purpose: Accepts and computer message and either return a computer that match
        ///          the computer information, or creates a new computer into the database
        /// 
        ///  POST api/Computer
        /// </summary>
        /// <param name="postComputer">an json computer object to send</param>
        /// <returns>
        ///An existing Computer either or from the just created 
        /// </returns>
        public Computer PostComputerEntity(Computer postComputer)
        {
            //PostResponse<int> postResponse = new PostResponse<int>();

            HttpResponseMessage response;



            if (ModelState.IsValid && !String.IsNullOrEmpty(postComputer.Name))
            {
                var existingComputer = db.ComputerEntities.Where(m => m.Building == postComputer.Building
                                                                    && m.Name == postComputer.Name
                                                                    && m.Room == postComputer.Room).FirstOrDefault();
                if (existingComputer != null)
                {
                    postComputer.ID = existingComputer.ID;
                    postComputer.isEnabled = existingComputer.isEnabled;
                    try
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, existingComputer);
                        response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = existingComputer.ID }));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Response is not required" + ex.ToString());
                    }
                }
                else
                {
                    ComputerEntity computerentity = new ComputerEntity()
                    {
                        Building = postComputer.Building,
                        Name = postComputer.Name,
                        Room = postComputer.Room,
                        isEnabled = true
                    };


                    db.ComputerEntities.AddObject(computerentity);
                    db.SaveChanges();


                    postComputer.ID = computerentity.ID;

                    response = Request.CreateResponse(HttpStatusCode.Created, computerentity);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = computerentity.ID }));
                }


                return postComputer;

                //postResponse.ReturnObject = postComputer.ID;
                //return postResponse;
            }

            try
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Response is not required" + ex.ToString());
            }


            return postComputer;

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}