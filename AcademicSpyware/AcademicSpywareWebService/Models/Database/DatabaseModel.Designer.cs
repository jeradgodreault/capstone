﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
#region EDM Relationship Metadata

[assembly: EdmRelationshipAttribute("SpywareDatabaseModel", "FK_History_Computer", "Computers", System.Data.Metadata.Edm.RelationshipMultiplicity.One, typeof(AcademicSpywareWebService.Models.Database.ComputerEntity), "Histories", System.Data.Metadata.Edm.RelationshipMultiplicity.Many, typeof(AcademicSpywareWebService.Models.Database.HistoryEntity), true)]
[assembly: EdmRelationshipAttribute("SpywareDatabaseModel", "FK_History_Status", "Status", System.Data.Metadata.Edm.RelationshipMultiplicity.One, typeof(AcademicSpywareWebService.Models.Database.StatusEntity), "Histories", System.Data.Metadata.Edm.RelationshipMultiplicity.Many, typeof(AcademicSpywareWebService.Models.Database.HistoryEntity), true)]

#endregion

namespace AcademicSpywareWebService.Models.Database
{
    #region Contexts
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    public partial class SpywareEntities : ObjectContext
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new SpywareEntities object using the connection string found in the 'SpywareEntities' section of the application configuration file.
        /// </summary>
        public SpywareEntities() : base("name=SpywareEntities", "SpywareEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new SpywareEntities object.
        /// </summary>
        public SpywareEntities(string connectionString) : base(connectionString, "SpywareEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new SpywareEntities object.
        /// </summary>
        public SpywareEntities(EntityConnection connection) : base(connection, "SpywareEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region Partial Methods
    
        partial void OnContextCreated();
    
        #endregion
    
        #region ObjectSet Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<ComputerEntity> ComputerEntities
        {
            get
            {
                if ((_ComputerEntities == null))
                {
                    _ComputerEntities = base.CreateObjectSet<ComputerEntity>("ComputerEntities");
                }
                return _ComputerEntities;
            }
        }
        private ObjectSet<ComputerEntity> _ComputerEntities;
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<HistoryEntity> HistoryEntities
        {
            get
            {
                if ((_HistoryEntities == null))
                {
                    _HistoryEntities = base.CreateObjectSet<HistoryEntity>("HistoryEntities");
                }
                return _HistoryEntities;
            }
        }
        private ObjectSet<HistoryEntity> _HistoryEntities;
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<StatusEntity> StatusEntities
        {
            get
            {
                if ((_StatusEntities == null))
                {
                    _StatusEntities = base.CreateObjectSet<StatusEntity>("StatusEntities");
                }
                return _StatusEntities;
            }
        }
        private ObjectSet<StatusEntity> _StatusEntities;
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<UserEntity> UserEntities
        {
            get
            {
                if ((_UserEntities == null))
                {
                    _UserEntities = base.CreateObjectSet<UserEntity>("UserEntities");
                }
                return _UserEntities;
            }
        }
        private ObjectSet<UserEntity> _UserEntities;
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<DetailHistoryView> DetailHistoryViews
        {
            get
            {
                if ((_DetailHistoryViews == null))
                {
                    _DetailHistoryViews = base.CreateObjectSet<DetailHistoryView>("DetailHistoryViews");
                }
                return _DetailHistoryViews;
            }
        }
        private ObjectSet<DetailHistoryView> _DetailHistoryViews;
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<View_BuildingWing> View_BuildingWing
        {
            get
            {
                if ((_View_BuildingWing == null))
                {
                    _View_BuildingWing = base.CreateObjectSet<View_BuildingWing>("View_BuildingWing");
                }
                return _View_BuildingWing;
            }
        }
        private ObjectSet<View_BuildingWing> _View_BuildingWing;

        #endregion

        #region AddTo Methods
    
        /// <summary>
        /// Deprecated Method for adding a new object to the ComputerEntities EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToComputerEntities(ComputerEntity computerEntity)
        {
            base.AddObject("ComputerEntities", computerEntity);
        }
    
        /// <summary>
        /// Deprecated Method for adding a new object to the HistoryEntities EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToHistoryEntities(HistoryEntity historyEntity)
        {
            base.AddObject("HistoryEntities", historyEntity);
        }
    
        /// <summary>
        /// Deprecated Method for adding a new object to the StatusEntities EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToStatusEntities(StatusEntity statusEntity)
        {
            base.AddObject("StatusEntities", statusEntity);
        }
    
        /// <summary>
        /// Deprecated Method for adding a new object to the UserEntities EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToUserEntities(UserEntity userEntity)
        {
            base.AddObject("UserEntities", userEntity);
        }
    
        /// <summary>
        /// Deprecated Method for adding a new object to the DetailHistoryViews EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToDetailHistoryViews(DetailHistoryView detailHistoryView)
        {
            base.AddObject("DetailHistoryViews", detailHistoryView);
        }
    
        /// <summary>
        /// Deprecated Method for adding a new object to the View_BuildingWing EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToView_BuildingWing(View_BuildingWing view_BuildingWing)
        {
            base.AddObject("View_BuildingWing", view_BuildingWing);
        }

        #endregion

    }

    #endregion

    #region Entities
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="SpywareDatabaseModel", Name="ComputerEntity")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class ComputerEntity : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new ComputerEntity object.
        /// </summary>
        /// <param name="id">Initial value of the ID property.</param>
        /// <param name="building">Initial value of the Building property.</param>
        /// <param name="room">Initial value of the Room property.</param>
        /// <param name="name">Initial value of the Name property.</param>
        /// <param name="isEnabled">Initial value of the isEnabled property.</param>
        public static ComputerEntity CreateComputerEntity(global::System.Int32 id, global::System.String building, global::System.String room, global::System.String name, global::System.Boolean isEnabled)
        {
            ComputerEntity computerEntity = new ComputerEntity();
            computerEntity.ID = id;
            computerEntity.Building = building;
            computerEntity.Room = room;
            computerEntity.Name = name;
            computerEntity.isEnabled = isEnabled;
            return computerEntity;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (_ID != value)
                {
                    OnIDChanging(value);
                    ReportPropertyChanging("ID");
                    _ID = StructuralObject.SetValidValue(value);
                    ReportPropertyChanged("ID");
                    OnIDChanged();
                }
            }
        }
        private global::System.Int32 _ID;
        partial void OnIDChanging(global::System.Int32 value);
        partial void OnIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Building
        {
            get
            {
                return _Building;
            }
            set
            {
                OnBuildingChanging(value);
                ReportPropertyChanging("Building");
                _Building = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("Building");
                OnBuildingChanged();
            }
        }
        private global::System.String _Building;
        partial void OnBuildingChanging(global::System.String value);
        partial void OnBuildingChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Room
        {
            get
            {
                return _Room;
            }
            set
            {
                OnRoomChanging(value);
                ReportPropertyChanging("Room");
                _Room = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("Room");
                OnRoomChanged();
            }
        }
        private global::System.String _Room;
        partial void OnRoomChanging(global::System.String value);
        partial void OnRoomChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Name
        {
            get
            {
                return _Name;
            }
            set
            {
                OnNameChanging(value);
                ReportPropertyChanging("Name");
                _Name = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("Name");
                OnNameChanged();
            }
        }
        private global::System.String _Name;
        partial void OnNameChanging(global::System.String value);
        partial void OnNameChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Boolean isEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                OnisEnabledChanging(value);
                ReportPropertyChanging("isEnabled");
                _isEnabled = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("isEnabled");
                OnisEnabledChanged();
            }
        }
        private global::System.Boolean _isEnabled;
        partial void OnisEnabledChanging(global::System.Boolean value);
        partial void OnisEnabledChanged();

        #endregion

    
        #region Navigation Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [XmlIgnoreAttribute()]
        [SoapIgnoreAttribute()]
        [DataMemberAttribute()]
        [EdmRelationshipNavigationPropertyAttribute("SpywareDatabaseModel", "FK_History_Computer", "Histories")]
        public EntityCollection<HistoryEntity> Histories
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedCollection<HistoryEntity>("SpywareDatabaseModel.FK_History_Computer", "Histories");
            }
            set
            {
                if ((value != null))
                {
                    ((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedCollection<HistoryEntity>("SpywareDatabaseModel.FK_History_Computer", "Histories", value);
                }
            }
        }

        #endregion

    }
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="SpywareDatabaseModel", Name="DetailHistoryView")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class DetailHistoryView : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new DetailHistoryView object.
        /// </summary>
        /// <param name="building">Initial value of the Building property.</param>
        /// <param name="room">Initial value of the Room property.</param>
        /// <param name="pC">Initial value of the PC property.</param>
        /// <param name="isEnabled">Initial value of the isEnabled property.</param>
        /// <param name="description">Initial value of the Description property.</param>
        public static DetailHistoryView CreateDetailHistoryView(global::System.String building, global::System.String room, global::System.String pC, global::System.Boolean isEnabled, global::System.String description)
        {
            DetailHistoryView detailHistoryView = new DetailHistoryView();
            detailHistoryView.Building = building;
            detailHistoryView.Room = room;
            detailHistoryView.PC = pC;
            detailHistoryView.isEnabled = isEnabled;
            detailHistoryView.Description = description;
            return detailHistoryView;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Building
        {
            get
            {
                return _Building;
            }
            set
            {
                if (_Building != value)
                {
                    OnBuildingChanging(value);
                    ReportPropertyChanging("Building");
                    _Building = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("Building");
                    OnBuildingChanged();
                }
            }
        }
        private global::System.String _Building;
        partial void OnBuildingChanging(global::System.String value);
        partial void OnBuildingChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Room
        {
            get
            {
                return _Room;
            }
            set
            {
                if (_Room != value)
                {
                    OnRoomChanging(value);
                    ReportPropertyChanging("Room");
                    _Room = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("Room");
                    OnRoomChanged();
                }
            }
        }
        private global::System.String _Room;
        partial void OnRoomChanging(global::System.String value);
        partial void OnRoomChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String PC
        {
            get
            {
                return _PC;
            }
            set
            {
                if (_PC != value)
                {
                    OnPCChanging(value);
                    ReportPropertyChanging("PC");
                    _PC = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("PC");
                    OnPCChanged();
                }
            }
        }
        private global::System.String _PC;
        partial void OnPCChanging(global::System.String value);
        partial void OnPCChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Boolean isEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if (_isEnabled != value)
                {
                    OnisEnabledChanging(value);
                    ReportPropertyChanging("isEnabled");
                    _isEnabled = StructuralObject.SetValidValue(value);
                    ReportPropertyChanged("isEnabled");
                    OnisEnabledChanged();
                }
            }
        }
        private global::System.Boolean _isEnabled;
        partial void OnisEnabledChanging(global::System.Boolean value);
        partial void OnisEnabledChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> LastUpdate
        {
            get
            {
                return _LastUpdate;
            }
            set
            {
                OnLastUpdateChanging(value);
                ReportPropertyChanging("LastUpdate");
                _LastUpdate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("LastUpdate");
                OnLastUpdateChanged();
            }
        }
        private Nullable<global::System.DateTime> _LastUpdate;
        partial void OnLastUpdateChanging(Nullable<global::System.DateTime> value);
        partial void OnLastUpdateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if (_Description != value)
                {
                    OnDescriptionChanging(value);
                    ReportPropertyChanging("Description");
                    _Description = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("Description");
                    OnDescriptionChanged();
                }
            }
        }
        private global::System.String _Description;
        partial void OnDescriptionChanging(global::System.String value);
        partial void OnDescriptionChanged();

        #endregion

    
    }
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="SpywareDatabaseModel", Name="HistoryEntity")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class HistoryEntity : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new HistoryEntity object.
        /// </summary>
        /// <param name="id">Initial value of the ID property.</param>
        /// <param name="lastUpdate">Initial value of the LastUpdate property.</param>
        /// <param name="computerID">Initial value of the ComputerID property.</param>
        /// <param name="statusID">Initial value of the StatusID property.</param>
        public static HistoryEntity CreateHistoryEntity(global::System.Int32 id, global::System.DateTime lastUpdate, global::System.Int32 computerID, global::System.Int32 statusID)
        {
            HistoryEntity historyEntity = new HistoryEntity();
            historyEntity.ID = id;
            historyEntity.LastUpdate = lastUpdate;
            historyEntity.ComputerID = computerID;
            historyEntity.StatusID = statusID;
            return historyEntity;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (_ID != value)
                {
                    OnIDChanging(value);
                    ReportPropertyChanging("ID");
                    _ID = StructuralObject.SetValidValue(value);
                    ReportPropertyChanged("ID");
                    OnIDChanged();
                }
            }
        }
        private global::System.Int32 _ID;
        partial void OnIDChanging(global::System.Int32 value);
        partial void OnIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.DateTime LastUpdate
        {
            get
            {
                return _LastUpdate;
            }
            set
            {
                OnLastUpdateChanging(value);
                ReportPropertyChanging("LastUpdate");
                _LastUpdate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("LastUpdate");
                OnLastUpdateChanged();
            }
        }
        private global::System.DateTime _LastUpdate;
        partial void OnLastUpdateChanging(global::System.DateTime value);
        partial void OnLastUpdateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 ComputerID
        {
            get
            {
                return _ComputerID;
            }
            set
            {
                OnComputerIDChanging(value);
                ReportPropertyChanging("ComputerID");
                _ComputerID = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("ComputerID");
                OnComputerIDChanged();
            }
        }
        private global::System.Int32 _ComputerID;
        partial void OnComputerIDChanging(global::System.Int32 value);
        partial void OnComputerIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 StatusID
        {
            get
            {
                return _StatusID;
            }
            set
            {
                OnStatusIDChanging(value);
                ReportPropertyChanging("StatusID");
                _StatusID = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("StatusID");
                OnStatusIDChanged();
            }
        }
        private global::System.Int32 _StatusID;
        partial void OnStatusIDChanging(global::System.Int32 value);
        partial void OnStatusIDChanged();

        #endregion

    
        #region Navigation Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [XmlIgnoreAttribute()]
        [SoapIgnoreAttribute()]
        [DataMemberAttribute()]
        [EdmRelationshipNavigationPropertyAttribute("SpywareDatabaseModel", "FK_History_Computer", "Computers")]
        public ComputerEntity Computer
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<ComputerEntity>("SpywareDatabaseModel.FK_History_Computer", "Computers").Value;
            }
            set
            {
                ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<ComputerEntity>("SpywareDatabaseModel.FK_History_Computer", "Computers").Value = value;
            }
        }
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [BrowsableAttribute(false)]
        [DataMemberAttribute()]
        public EntityReference<ComputerEntity> ComputerReference
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<ComputerEntity>("SpywareDatabaseModel.FK_History_Computer", "Computers");
            }
            set
            {
                if ((value != null))
                {
                    ((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<ComputerEntity>("SpywareDatabaseModel.FK_History_Computer", "Computers", value);
                }
            }
        }
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [XmlIgnoreAttribute()]
        [SoapIgnoreAttribute()]
        [DataMemberAttribute()]
        [EdmRelationshipNavigationPropertyAttribute("SpywareDatabaseModel", "FK_History_Status", "Status")]
        public StatusEntity Status
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<StatusEntity>("SpywareDatabaseModel.FK_History_Status", "Status").Value;
            }
            set
            {
                ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<StatusEntity>("SpywareDatabaseModel.FK_History_Status", "Status").Value = value;
            }
        }
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [BrowsableAttribute(false)]
        [DataMemberAttribute()]
        public EntityReference<StatusEntity> StatusReference
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<StatusEntity>("SpywareDatabaseModel.FK_History_Status", "Status");
            }
            set
            {
                if ((value != null))
                {
                    ((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<StatusEntity>("SpywareDatabaseModel.FK_History_Status", "Status", value);
                }
            }
        }

        #endregion

    }
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="SpywareDatabaseModel", Name="StatusEntity")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class StatusEntity : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new StatusEntity object.
        /// </summary>
        /// <param name="id">Initial value of the ID property.</param>
        /// <param name="description">Initial value of the Description property.</param>
        public static StatusEntity CreateStatusEntity(global::System.Int32 id, global::System.String description)
        {
            StatusEntity statusEntity = new StatusEntity();
            statusEntity.ID = id;
            statusEntity.Description = description;
            return statusEntity;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (_ID != value)
                {
                    OnIDChanging(value);
                    ReportPropertyChanging("ID");
                    _ID = StructuralObject.SetValidValue(value);
                    ReportPropertyChanged("ID");
                    OnIDChanged();
                }
            }
        }
        private global::System.Int32 _ID;
        partial void OnIDChanging(global::System.Int32 value);
        partial void OnIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Description
        {
            get
            {
                return _Description;
            }
            set
            {
                OnDescriptionChanging(value);
                ReportPropertyChanging("Description");
                _Description = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("Description");
                OnDescriptionChanged();
            }
        }
        private global::System.String _Description;
        partial void OnDescriptionChanging(global::System.String value);
        partial void OnDescriptionChanged();

        #endregion

    
        #region Navigation Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [XmlIgnoreAttribute()]
        [SoapIgnoreAttribute()]
        [DataMemberAttribute()]
        [EdmRelationshipNavigationPropertyAttribute("SpywareDatabaseModel", "FK_History_Status", "Histories")]
        public EntityCollection<HistoryEntity> Histories
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedCollection<HistoryEntity>("SpywareDatabaseModel.FK_History_Status", "Histories");
            }
            set
            {
                if ((value != null))
                {
                    ((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedCollection<HistoryEntity>("SpywareDatabaseModel.FK_History_Status", "Histories", value);
                }
            }
        }

        #endregion

    }
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="SpywareDatabaseModel", Name="UserEntity")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class UserEntity : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new UserEntity object.
        /// </summary>
        /// <param name="username">Initial value of the username property.</param>
        /// <param name="password">Initial value of the password property.</param>
        public static UserEntity CreateUserEntity(global::System.String username, global::System.String password)
        {
            UserEntity userEntity = new UserEntity();
            userEntity.username = username;
            userEntity.password = password;
            return userEntity;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String username
        {
            get
            {
                return _username;
            }
            set
            {
                if (_username != value)
                {
                    OnusernameChanging(value);
                    ReportPropertyChanging("username");
                    _username = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("username");
                    OnusernameChanged();
                }
            }
        }
        private global::System.String _username;
        partial void OnusernameChanging(global::System.String value);
        partial void OnusernameChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String password
        {
            get
            {
                return _password;
            }
            set
            {
                OnpasswordChanging(value);
                ReportPropertyChanging("password");
                _password = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("password");
                OnpasswordChanged();
            }
        }
        private global::System.String _password;
        partial void OnpasswordChanging(global::System.String value);
        partial void OnpasswordChanged();

        #endregion

    
    }
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="SpywareDatabaseModel", Name="View_BuildingWing")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class View_BuildingWing : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new View_BuildingWing object.
        /// </summary>
        /// <param name="buildingID">Initial value of the BuildingID property.</param>
        /// <param name="wing">Initial value of the Wing property.</param>
        public static View_BuildingWing CreateView_BuildingWing(global::System.String buildingID, global::System.String wing)
        {
            View_BuildingWing view_BuildingWing = new View_BuildingWing();
            view_BuildingWing.BuildingID = buildingID;
            view_BuildingWing.Wing = wing;
            return view_BuildingWing;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String BuildingID
        {
            get
            {
                return _BuildingID;
            }
            set
            {
                if (_BuildingID != value)
                {
                    OnBuildingIDChanging(value);
                    ReportPropertyChanging("BuildingID");
                    _BuildingID = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("BuildingID");
                    OnBuildingIDChanged();
                }
            }
        }
        private global::System.String _BuildingID;
        partial void OnBuildingIDChanging(global::System.String value);
        partial void OnBuildingIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Wing
        {
            get
            {
                return _Wing;
            }
            set
            {
                if (_Wing != value)
                {
                    OnWingChanging(value);
                    ReportPropertyChanging("Wing");
                    _Wing = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("Wing");
                    OnWingChanged();
                }
            }
        }
        private global::System.String _Wing;
        partial void OnWingChanging(global::System.String value);
        partial void OnWingChanged();

        #endregion

    
    }

    #endregion

    
}
