﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace AcademicSpywareWebService.Models
{

    /// <summary>
    /// Purpose: A json friendly status to be used to translate complex entity framework status
    /// </summary>
    public class Status
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    ///     Purpose: holds the httpresponse and the status that was just added. 
    ///     
    /// </summary>
    public class StatusAdd
    {
        public HttpStatusCode responseCode { get; set; }
        public String responseMessage { get; set; }
        public Status status {get; set;}
    }


    /// <summary>
    /// Purpose: Holds the a httpresponse message and computer that was
    ///          just added
    /// </summary>
    public class ComputerAdd
    {
        public HttpResponseMessage httpResponseMessage {get;set;}
        public Computer computer {get; set;}
    }


    /// <summary>
    /// TODO: Remove can;t send unknown generic object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PostResponse<T>
    {
        public HttpResponseMessage ResponseMessage { get; set; }
        public T ReturnObject {get; set;}
    }

    /// <summary>
    /// Purpose: A json friendly computer to be used to translate complex entity framework computer
    /// </summary>
    public class Computer
    {
        public int ID { get; set; }
        public string Building { get; set; }
        public string Room { get; set; }
        [Required]
        public string Name { get; set; }
        public bool isEnabled { get; set; }
    }

    /// <summary>
    /// Purpose: A json friendly history to be used to translate complex entity framework history
    /// </summary>
    public class History
    {
        public int ID { get; set; }
        public DateTime LastUpdate { get; set; }
        public int ComputerID { get; set; }
        public int StatusID { get; set; }
    }
   
}