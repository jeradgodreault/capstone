﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcademicSpyware.Models.ViewModel
{
    /// <summary>
    ///     Purpose: View Model to hold a history event and list of building.
    /// </summary>
    public class DetailRoomView
    {

        public List<DetailHistoryView> DetailHistory { get; set; }
        public List<View_BuildingWing> BuildingWing { get; set; }


        public DetailRoomView()
        {
            DetailHistory = new List<DetailHistoryView>();
            BuildingWing = new List<View_BuildingWing>();
        }

    }
}