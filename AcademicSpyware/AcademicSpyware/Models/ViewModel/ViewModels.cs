﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcademicSpyware.Models.ViewModel
{

    /// <summary>
    ///     Purpose: To hold a series of sections and Buildings
    /// </summary>
    public class DetailViewModel
    {
        public List<SectionViewModel> section { get; set; }
        public List<View_BuildingWing> BuildingWing { get; set; }
        public String building {get;set;}

        public DetailViewModel()
        {
            section = new List<SectionViewModel>();
            BuildingWing = new List<View_BuildingWing>();
        }

    }

    /// <summary>
    /// Purpose: To create sections of history based on an Room. 
    ///          So [items] would contain a detailHistoryView for a 
    ///          specfic room. i.e Room A102
    /// </summary>
    public class SectionViewModel
    {
        public List<DetailHistoryView> items { get; set; }

        public SectionViewModel()
        {
            items = new List<DetailHistoryView>();
        }
    }
}