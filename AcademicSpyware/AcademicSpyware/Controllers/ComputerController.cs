﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcademicSpyware.Models;
using AcademicSpyware.Models.ViewModel;

namespace AcademicSpyware.Controllers
{
    public class ComputerController : Controller
    {
        private SpywareDBEntities db = new SpywareDBEntities();

        /// <summary>
        ///     Purpose: Shows a list of wings
        /// </summary>
        /// <returns>GET: /Computer/</returns>
        public ActionResult Index()
        {
            return View(db.View_BuildingWing.ToList());
        }

        /// <summary>
        ///     Purpose: Shows a list of history based on the building ID
        /// </summary>
        /// <param name="ID">Building Name</param>
        /// <returns> GET: /Computer/DetailRoom/ID</returns>
        public ActionResult DetailRoom(string ID)
        {
            var result = db.DetailHistoryViews.Where(w => w.Building == ID).ToList();

            return View(result);
        }

        /// <summary>
        ///     Purpose: Shows a history based on the building ID and seperate 
        ///              the Rooms into the DetailViewModel
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>GET: /Computer/Building/ID</returns>
        public ActionResult Building(string ID)
        {
            var result = db.DetailHistoryViews.Where(w => w.Building == ID).ToList();
            var building = db.View_BuildingWing.ToList();

            DetailViewModel dvm = new DetailViewModel();

            foreach (var i in result.Select(w => w.Room).Distinct())
            {
               var sectionItem  = result.Where(c => c.Room == i);

               dvm.section.Add(new SectionViewModel() { items = sectionItem.OrderByDescending(c => c.Description).ToList() });
            }

            dvm.BuildingWing = building;

            dvm.building = ID;

            return View(dvm);
        }

    }
}
