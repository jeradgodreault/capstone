﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;

using System.Threading.Tasks;
using AcademicSpywareWebService.Models;
using System.Diagnostics;

namespace AcademicSpywareForms.RestRequests
{
    /// <summary>
    ///     Purpose: Represents a intermediate between the controller and the Rest Client
    ///              This will handle any formatting request to determine which object it
    ///              expect using RequestType
    /// </summary>
    class Requests
    {
        private String baseUrl = "http://spywareservice.azurewebsites.net";
        private String Api_Status = "api/status";
        private String Api_Computer = "api/computer";
        private String Api_History = "api/History";

        public RestClient restClient {get; private set;}

        /// <summary>
        /// Purpose: Testing, see if we can get all the status
        /// </summary>
        public void GetAllStatus()
        {
            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;

            restClient.resourcePath = this.Api_Status;
            restClient.httpProtocol = ProtocolType.GET;

            restClient.Execute();
        }

        /// <summary>
        /// Purpose: Get all the status using C# Async 
        /// </summary>
        /// <returns>an List of Status</returns>
        public async Task<List<Status>> GetAllStatusAsync()
        { 

            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;

            restClient.resourcePath = this.Api_Status;
            restClient.httpProtocol = ProtocolType.GET;

            var e = await restClient.ExecuteAsync();

            var x = await e.Content.ReadAsAsync<IEnumerable<Status>>();

            return x.ToList();
        }


        /// <summary>
        /// Purpose: POST an JSON of status to webservice
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<StatusAdd> PostStatusAsync(Status status)
        {
            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;

            restClient.resourcePath = this.Api_Status;
            restClient.httpProtocol = ProtocolType.POST;
            restClient.postObject = status;


            var c = await restClient.ExecuteAsync();

            var d = await c.Content.ReadAsAsync<StatusAdd>();

            return d;

            //var executeResponse = await restClient.ExecuteAsync().Result.Content.ReadAsAsync<StatusAdd>();

            //return executeResponse;
        }


        /// <summary>
        /// Purpose: A newer version to send Computer Json to the webservice
        /// </summary>
        /// <param name="computer">A new Computer to be inserted into the database</param>
        /// <returns>Computer returned from the webservice containg </returns>
        public async Task<Computer> PostComputerAsyncVersionTwo(Computer computer)
        {
            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;

            restClient.resourcePath = this.Api_Computer;
            restClient.httpProtocol = ProtocolType.POST;
            restClient.postObject = computer;

            var c = await restClient.ExecuteAsync();

            var r = await c.Content.ReadAsAsync<Computer>();

            return r;

            //var executeResponse = await restClient.ExecuteAsync().Result.Content.ReadAsAsync<Computer>();

            //return executeResponse;
        }

        /// <summary>
        /// Purpose: POSTS an history event to the webservice
        /// </summary>
        /// <param name="computer">an history event with a certain status</param>
        public async void PostHistoryAsync(History computer)
        {
            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;

            restClient.resourcePath = this.Api_History;
            restClient.httpProtocol = ProtocolType.POST;
            restClient.postObject = computer;

            HttpResponseMessage x = await restClient.ExecuteAsync();

            var history = await x.Content.ReadAsAsync<History>();

            if (x.IsSuccessStatusCode)
            {
                Debug.WriteLine("success! history submitted {0} for computer {1} was updated", history.ID, history.ComputerID);
            }
            else
            {
                Debug.WriteLine("failed history submitted" + x.RequestMessage.Content.ToString());
            }

       
        }

        /// <summary>
        /// TODO: REMOVE
        /// </summary>
        /// <param name="computer"></param>
        /// <returns></returns>
        public async Task<PostResponse<int>> PostComputerAsync(Computer computer)
        {
            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;

            restClient.resourcePath = this.Api_Computer;
            restClient.httpProtocol = ProtocolType.POST;
            restClient.postObject = computer;

            var x = await restClient.ExecuteAsync();

            var executeResponse = await x.Content.ReadAsAsync<PostResponse<int>>();

            return executeResponse;
        }

        /// <summary>
        /// TODO: Remove
        /// </summary>
        /// <param name="computer"></param>
        public void PostComputer(Computer computer)
        {
            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;
            restClient.resourcePath = this.Api_Computer;
            restClient.httpProtocol = ProtocolType.POST;
            restClient.postObject = computer;

            restClient.Execute();
        }



        /// <summary>
        /// Purpose: Gets all the computers async from the webservice
        /// </summary>
        /// <returns>A list of computers from the webserive</returns>
        public async Task<List<Computer>> GetAllComputerAsync()
        {
            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;

            restClient.resourcePath = this.Api_Computer;
            restClient.httpProtocol = ProtocolType.GET;

            var x = await restClient.ExecuteAsync();


            var executeResponse = await x.Content.ReadAsAsync<IEnumerable<Computer>>();

            return executeResponse.ToList();
        }


        /// <summary>
        /// TODO: REMOVE
        /// </summary>
        /// <param name="status"></param>
        public void PostStatus(Status status)
        {
            restClient = new RestClient();
            restClient.baseUrl = this.baseUrl;

            restClient.resourcePath = this.Api_Status;
            restClient.httpProtocol = ProtocolType.POST;

            restClient.postObject = status;
            restClient.Execute();
        }

    }


}
